const bcrypt = require("bcrypt");

function hash(data){
    let encriptado = bcrypt.hashSync(data, 10);
    console.log("Encriptando los datos " + encriptado)
    return encriptado;
}
function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
    if (passwordFromUserInPlainText === passwordFromDBHashed) {
        console.log("password sin hash. Comparando");
        return passwordFromUserInPlainText === passwordFromDBHashed;
    } else {
        return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
    }
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
