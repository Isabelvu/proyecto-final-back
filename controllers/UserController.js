//aqui pongo los modulos que utilizamos como llamamos a writeUserDatatoFile tenemos que declararlo arriba
const io = require('../io');
const crypt = require("../crypt");
const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyectofinal/collections/";
const mLabAPIKey = "apiKey=" + process.env.mLabAPIKey;

//RECUPERO USUARIOS

function getUsersPr(req, res) {
    console.log("GET /apitechu/proyecto/user");
//  console.log("GET /apitechu/v2/users");
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Obtenemos el listado de usuarios");
    console.log("APIKEy " + mLabAPIKey);
//obtener toda la lista de usuarios. solo poner user y el apikey
    httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body) {
            var response = !err ? body :{
                "msg" : "Error obteniendo usuarios"
            }
            res.send(response);
        })}

// RECUPERO USER POR ID
function getUsersIdPr(req, res) {
    console.log("GET /apitechu/proyecto/users/:id");
    var id = req.params.id;
    var query = 'q={"id":' + id + '}';
    console.log("la consulta es " + query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado con get users de proyecto");
    //obtener toda la lista de usuarios. solo poner user y el apikey ..'q={"id":3}'
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
            if (err) {
                var response = {
                    "msg" : "Error obteniendo usuarios"
                }
                res.status(500);
            }else {
                if (body.length > 0) {
                    var response = body[0];
                }else {
                    var response = {
                        "msg" : "Usuario nop encontrado"
                    }
                    res.status(404);
                }
            }
            res.send(response);
        }
    )
}




//RECUPERO EL ULTIMO ID DE CLIENTE PARA EN LA CREACIÓN DE USUARIOS CREAR UNO MAS

function getUsersPrUltimoID(req, res) {
    console.log("GET /apitechu/proyecto/user");
//  console.log("GET /apitechu/v2/users");
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Obtenemos el listado de usuarios");
    console.log("APIKEy " + mLabAPIKey);
//obtener toda la lista de usuarios. solo poner user y el apikey
    httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body) {
            var response = !err ? body :{
                "msg" : "Error obteniendo usuarios"
            }
            res.send(response);
        })}


// Crear usuarios desde proyecto en MLAB
function createUsersPr(req, res) {
    console.log("POST /apitechu/proyecto/users");
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);
    console.log(req.body.genero);
    var idUser = new Date().valueOf();
    console.log(idUser);
    var newUser = {
        "id"  : idUser,
        "first_name": req.body.first_name,
        "last_name":req.body.last_name,
        "email":req.body.email,
        "genero":req.body.genero,
        "password": crypt.hash(req.body.password),
    }

    // vemos si usuario existe. Si ya existe mostramos un error.
    var query = 'q={"email": "' + req.body.email + '"}';
    console.log("haciendo login de usuario " + query);
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("URL:" + baseMLabURL + "user?" + query + "&" + mLabAPIKey);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
            if (err || body.length > 0) {
                var response = {
                    "mensaje": "Error creando usuario"
                }
                res.status(500);
                res.send(response);
            } else {
                var httpClient = requestJson.createClient(baseMLabURL);
                console.log("Cliente creado con proyecto" + mLabAPIKey);
                httpClient.post("user?" + mLabAPIKey, newUser,
                    function (err, resMLab, body) {
                        console.log("Usuario creado en mLabAPIKey");
                        res.status(201).send({"msg": "Movimiento creado"});
                    }
                );
            }
        });
}


// POR AHORA NO PERMITO LA FUNCIONALIDAD DE BORRAR USUARIOS



//
// // borrado de usuario desde proyecto
//  function deleteUserPr(req, res){
//    console.log("DELETE/apitechu/proyecto/users/:id");
//    console.log("Id es el " + req.params.id);
// //obtenemos el listado de usuarios
//    var httpClient = requestJson.createClient(baseMLabURL);
// console.log("Obtenemos el listado de usuarios");
//
//
//    var users = require('../usuarios.json');
//    var deleted = false;
//    var i;
//    for (i = 0; i < users.length; i++)  {
//      console.log("comparando " + users[i].id + " y " + req.params.id);
//      console.log("Me quedan los siguientes usuarios en el array" + users.length);
// //    length	Sets or returns the number of elements in an array
//       if (users[i].id == req.params.id) {
//       console.log("parametro de la URL " + req.params.id);
//   //    quiero borrar ese parametro
//           console.log("paso por splice y el indice vale " + i);
//           users.splice(i,1);
//           io.writeUserDatatoFile(users);
//           res.send({"msg" : "Usuario borrado con exito por postman"})
//           console.log("Usuario borrado ok");
//           deleted = true;
//           break; // se sale de aqui con el break
//    }
//  };
//  }


// EXPORTS del proyecto
module.exports.getUsersPr = getUsersPr ;
module.exports.getUsersIdPr = getUsersIdPr ;
module.exports.createUsersPr = createUsersPr ;
//module.exports.deleteUserPr = deleteUserPr ;
// module.exports.getUsersPrUltimoID = getUsersPrUltimoID ;
