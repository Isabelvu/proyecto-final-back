//Creamos un nuevo controller para hacer el login y el logout
const io = require('../io');
const crypt = require("../crypt");
const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyectofinal/collections/";
const mLabAPIKey = "apiKey=" + process.env.mLabAPIKey;
// practica 2 login


function loginUsersPr(req, res) {
    console.log("POST /apitechu/proyecto/login");
    var query = 'q={"email": "' + req.body.email + '"}';
    var password = req.body.password;
    console.log("haciendo login de usuario " + query);
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("URL:" + baseMLabURL + "user?" + query + "&" + mLabAPIKey);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
            if (err) {
                var response = {
                    "mensaje" : "Error en login"
                }
                res.status(500);
                res.send(response);
            } else if (body.length > 0) {
                var isPasswordcorrect = crypt.checkPassword(password, body[0].password);
                console.log("Password correct is " + isPasswordcorrect);
                if (!isPasswordcorrect) {
                    var response = {
                        "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
                    }
                    res.status(401);
                    res.send(response);
                } else {

                    ///user?q={"id" :'+body[0].id +'}&{"$set":{"logged":true}} &apiKey=myAPIKey

                    console.log("Got a user with that email and password, logging in");
                    query = 'q={"id" : ' + body[0].id +'}';
                    console.log("Query for put is " + query);
                    var putBody = '{"$set":{"logged":true}}';
                    console.log("URL PUT:" + baseMLabURL + "user?" + query + "&" + mLabAPIKey);
                    httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                        function(errPUT, resMLabPUT, bodyPUT) {
                            console.log("PUT realizado: Se graba el logged a true");
                            if (body[0].genero === "Mujer") {
                                var generico = "Bienvenida de nuevo señora";
                            }else {
                                var generico = "Bienvenido de nuevo señor";
                            }
                            var response = {
                                "msg" : "Usuaria logadica con éxito",
                                "idUsuario" : body[0].id,
                                "Nombre" : body[0].first_name,
                                "Apellido": body[0].last_name,
                                "Genero" : body[0].genero,
                                generico,
                            }
                            console.log(response);
                            res.send(response);

                        }
                    )
                }
            } else {
                var response = {
                    "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
                }
                res.status(404);
                res.send(response);
            }

        }
    );
}


function logoutUsersPr(req, res) {
    console.log("POST /apitechu/proyecto/logout/:id");

    var query = 'q={"id": ' + req.params.id + '}';
    console.log("query es " + query);

    httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
            if (body.length == 0) {
                var response = {
                    "mensaje" : "Logout incorrecto, usuario no encontrado"
                }
                res.send(response);
            } else {
                if (body[0].logged=="false"){
                    var response = {
                        "mensaje" : "usuario no estaba logado"
                    }
                }else
                    console.log("Got a user with that id, logging out");
                console.log("Got xcvxcva " + body[0].logged);
                query = 'q={"id" : ' + body[0].id +'}';
                console.log("Query for put is " + query);
                var putBody = '{"$unset":{"logged":""}}'
                httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(errPUT, resMLabPUT, bodyPUT) {
                        console.log("PUT done");
                        var response = {
                            "msg" : "Usuario deslogado",
                            "idUsuario" : body[0].id
                        }
                        res.send(response);
                    }
                )
            }
        }
    );
}

module.exports.loginUsersPr = loginUsersPr ;
module.exports.logoutUsersPr = logoutUsersPr ;
