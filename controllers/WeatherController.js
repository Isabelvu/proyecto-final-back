const requestJson = require("request-json");

function weather(req, res) {
    console.log("consultar el tiempo en openweathermap")
    const baseWeatherURL = "https://api.openweathermap.org/data/2.5/";

    console.log("POST https://api.openweathermap.org/data/2.5/weather?lat=[[latitude]]&lon=[[longitude]]&APPID=3b4d5bbb3f4dc709d8d567ceaed2bb77");
    console.log(JSON.stringify(req.body))
    var query = `lat=${req.body.latitude}&lon=${req.body.longitude}&APPID=3b4d5bbb3f4dc709d8d567ceaed2bb77`;
    console.log("query de weather es " + query);

    httpClient = requestJson.createClient(baseWeatherURL);
    httpClient.get("weather?" + query,
        function(err, _res, body) {
            if (body.length == 0) {
                console.log("consultar el tiempo en openweathermap falla " + JSON.stringify(err));
                var response = {
                    "mensaje" : "No pudimos acceder al tiempo"
                }
                res.send(response);
            } else {
                console.log('tiempo pillado ' + JSON.stringify(body));
                res.send(body);
            }
        }
    );
}

module.exports.weather = weather ;
