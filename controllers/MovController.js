const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyectofinal/collections/";
const mLabAPIKey = "apiKey=" + process.env.mLabAPIKey;
//Conseguimos loas movimientos de una cuenta de un usuario

// function getMovimientosbyCC (req, res) {
//     console.log("GET /  apitechu/proyecto/movimientos/:id");
//     console.log("hola mas id del params" + req.params.id);
//     var idUsuario =req.params.id;
//     var query = 'q={"id":' + idUsuario + '}';
// // el idUsuario entre comillas es lo que traigo de mlab
//     console.log("la consulta es: " + query);
//     var httpClient = requestJson.createClient(baseMLabURL);
//     console.log("Cliente HTTP creado de getCuentasById ");
//     //obtener toda la lista de usuarios. solo poner user y el apikey
//     httpClient.get("movimientos?" + query + "&" + mLabAPIKey,
//         //"cuentas --> nombre de la coleccion"
//         function(err, resMLab, body) {
//             var response = !err ? body :{
//                 "msg" : "Error obteniendo movimientos"
//             }
//             res.send(response);
//         })}

//Conseguimos loas movimientos de una cuenta de un usuario
//  q={"id_usuario":7,"id_cuenta":7}
// ?id_usuario=7&id_cuenta=7

function getMovimientosbyCC (req, res) {
    console.log("GET /  apitechu/proyecto/movimientos/:id");
    console.log("hola mas id del params" + req.params.id_cuenta);
    var id = req.params.id_cuenta;
    var query = 'q={"id":' + id + '}';
    // el idUsuario entre comillas es lo que traigo de mlab
    console.log("la consulta es: " + query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado de getCuentasById ");
    //obtener toda la lista de usuarios. solo poner user y el apikey
    httpClient.get("movimientos?" + query + "&" + mLabAPIKey,
        //"cuentas --> nombre de la coleccion"
        function(err, resMLab, body) {
            var response = !err ? body :{
                "msg" : "Error obteniendo movimientos"
            }
            res.send(response);
        });
}

//Dar de alta un abono y sumar el saldo a la cuenta
// Crear usuarios desde proyecto en MLAB


/// insertar movimiento en la colección movimientos
function putAbonobyCC(req, res) {
    console.log("POST /  apitechu/proyecto/movimientos/crear");
    console.log(req.body.fecha);
    console.log(req.body.concepto);
    console.log(req.body.importe);
    var idmto = new Date().valueOf();
    console.log("id movimiento" + idmto);
    var newMvto = {
        "id_mto": idmto,
        "fecha": req.body.fecha,
        "concepto":req.body.concepto,
        "importe":req.body.importe,
    }
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Movimiento creado con proyecto" + mLabAPIKey);
    httpClient.post("movimientos?" +  mLabAPIKey, newMvto,
        function(err, resMLab, body) {
            res.status(201).send({"msg":"movimiento creadoo"});
        }
    )
}

function putIngresoCC(req, res) {
    var idCuenta = req.body.id_cuenta;
    var idUsuario = req.    body.id_usuario;
    var cantidad = req.body.cantidad;
    var query = 'q={"id":' + idCuenta + '}';

    var httpClient = requestJson.createClient(baseMLabURL);

    httpClient.get(`cuenta?${query}&${mLabAPIKey}`, function(err, resMLab, body) {

        //"cuentas --> nombre de la coleccion"
        if (err || body.length == 0) {
            res.send({
                "code": 404,
                "msg" : "Error obteniendo cuenta para ingreso"
            });
            return;
        }
        body[0].saldo += cantidad;
        let clientPostCuenta = requestJson.createClient(baseMLabURL);
        clientPostCuenta.post(`cuenta?${mLabAPIKey}`, body[0], function(err, resMLab, body) {
            if (err) {
                res.send({
                    "code": 404,
                    "msg": "Error obteniendo cuenta para ingreso"
                });
                return;
            }
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd < 10) {
                dd='0'+dd
            }
            if(mm < 10) {
                mm='0'+mm
            }
            var concepto = "abono";
            if (cantidad < 0) {
                cantidad *= -1;
                concepto = "cargo";
            }

            var movimiento = {
                id: new Date().valueOf(),
                id_usuario: idUsuario,
                id_cuenta: idCuenta,
                fecha: dd+'/'+mm+'/'+yyyy,
                concepto: concepto,
                importe: cantidad
            };
            let clientPostMovimiento = requestJson.createClient(baseMLabURL);
            clientPostMovimiento.post(`movimientos?${mLabAPIKey}`, movimiento, function(err, resMLab, body) {
                if (err) {
                    res.send({
                        "code": 404,
                        "msg": "Error obteniendo cuenta para ingreso"
                    });
                    return;
                }
                res.send({
                    "code": 200,
                    "msg": "Movimiento añadido"
                });
            });
        });
    });
}

module.exports.getMovimientosbyCC = getMovimientosbyCC ;
module.exports.putAbonobyCC = putAbonobyCC ;
module.exports.putIngresoCC = putIngresoCC;
