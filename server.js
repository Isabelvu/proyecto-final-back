require("dotenv").config();

const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");

  next();
};

app.use(express.json());
app.use(enableCORS);

const userController = require("./controllers/UserController");
const authController = require("./controllers/AuthController");
const cuentasController = require("./controllers/CuentasController");
const movController = require("./controllers/MovController");
const weatherController = require("./controllers/WeatherController");

// REGISTRO LAS RUTAS del proyecto
// userController
app.get("/apitechu/proyecto/users", userController.getUsersPr);
app.get("/apitechu/proyecto/users/:id", userController.getUsersIdPr);
app.put("/apitechu/proyecto/users/crear", userController.createUsersPr);
//app.delete("/apitechu/proyecto/users/:id", userController.deleteUserPr);
// authController
app.post("/apitechu/proyecto/login", authController.loginUsersPr);
app.get("/apitechu/proyecto/cuentas/:id", cuentasController.getCuentasByIdPr);
app.get("/apitechu/proyecto/logout/:id", authController.logoutUsersPr);
//movController
app.get("/apitechu/proyecto/movimientos/:id_cuenta", movController.getMovimientosbyCC);
app.put("/apitechu/proyecto/movimientos/crear", movController.putIngresoCC);

app.post("/apitechu/proyecto/weather", weatherController.weather);


app.listen(port);
console.log("API escuchando en el puerto cambio2 " + port);
